const json = '[{"gorodische": ["2d469a9e-8d32-43d6-b48a-834d9907c08f", "59d3c300-ded0-4906-958c-0eb5fbbdcd38", "aa568588-e86b-4473-bf59-facd6bdd5f8f", "982e2a03-0ee4-4ced-b556-b736b04bf4cb", "be1926c4-eeb5-439a-8204-d93ed42c8297", "d1469f56-36f4-4b33-b109-5c3832b17a87", "1d1000c7-f61c-4c65-aa73-7605ca45e830", "a79979ec-4f1b-4147-b9bb-e2596ca69be7" ], "byhanov": [ "dfefe46f-faa1-40b8-a1f6-3981b2a7b5e6", "b52528fc-1d7a-4e98-80ac-e1562440c2b1", "7af3f5d6-3d42-4980-8afd-bafd12267602", "c7b8bc01-9cb4-4dc7-8710-3a22e3f774ea", "0ed3d1f2-210d-4f55-967a-4514af121c7b", "37927418-af8f-4296-a56e-c290d10969fd", "c9a33076-6697-4539-8212-85231d1c1cd7", "00dfe131-4db7-40ac-a0e4-a9d2a45d8cd6", "c2952a14-f7c4-4879-b724-c0ae28b95080", "b202efe4-64ed-44b2-8282-7b4ef8a7bb84", "97025375-ea51-4dea-b982-2bf0fe50c87e", "98bb5c69-cbb3-4698-8dce-17ae9ab923ce", "eacb278b-46de-4602-8701-6beee61c4ce6", "dc541b9f-7282-4f77-9878-c2d89fca4244", "89342319-3e49-4411-a2c6-bb5ed4f7b908", "7c30ea92-1b00-4e7c-be61-2ad1fb058bc6", "17f8cfa2-ae0c-47d1-be88-e5e3fc9fadfc", "a6522a23-6109-473b-9a1f-a34f5656f913", "37d71aba-dbe3-4ce0-9ac2-2a32cfcbf567", "17e7c75f-824e-464b-b100-15905fd69a52", "86cb1b81-cf23-45a0-bd2b-5168f511008d", "c3186d78-3f7a-419f-b09a-81f6f57ad551", "ebefc55f-a378-4b9a-ae05-424f40c9a7a7", "03e6d3b8-f813-4fb1-818d-5063a79d3440", "eaf54ba1-cd6e-4da1-9551-c48b9ceb2f74", "b2c1fd37-4a13-4020-89fc-3d2d0a7604f5", "d2b88c3f-181b-49d6-9367-15fc1aba4065", "e148f6ca-71f5-4a4d-ad4d-caa4f9268f71", "dc04f150-7e94-4b81-8005-0c1464793a4d", "2007e71f-f91f-4153-a7ac-eaf75d711ff7", "8a91d749-cbcf-4adb-9549-496ca3c6b5e3", "49c276ec-9626-453c-9597-18f2fc93f8f9", "a98de7d6-45c3-4c37-935a-16a3d79217c4", "42650832-36af-44f4-a582-4ecf404b2e2b", "08b5837f-bea8-4f14-95d1-7d899de5ce09", "dd5dc495-dbd4-4e24-9f51-14741aa61aa5", "dc36a62c-3665-41a6-9c6b-38094cc181cc", "38c23aae-183f-4225-9def-935f4c1b5353", "48a30b5e-db8e-4291-84da-5f7a48afeab0", "3228962e-1a21-4395-9f99-0978336fc694", "f06ff6af-1a87-4120-b243-ea319be5cbf4", "cf23b621-e8b4-4ac9-bd8d-5fc9e5a0b7df", "662ed070-1cd7-49a1-9a96-de2f0b9a6581", "754ec598-2836-46c4-807a-69667ab7f34e", "d3d893a2-20b0-4171-86cf-696c84d8e1da", "a0937f44-e25a-4a91-9d26-10ab5b00c9a7", "6682020d-39bc-44c9-9797-f04aab2f7e10", "49036143-8050-4a75-abbe-b5f840358c16", "ab09eaa1-2a78-42a8-adae-04504cc4ba1f", "4fa2b630-852b-4df3-9008-3c7a8250303c", "129d9cc2-8951-4b4a-ac82-f918f913a1db", "1d767530-dbe9-49b0-9245-fa3bc84ccefe", "340d5053-6981-4a9f-aa26-2e21883ee7ad", "ebda3361-108d-4d92-8419-4cc02f524808" ]}]';
const btnsNext = document.querySelector('.btn_next');
const btnBack = document.querySelector('.btn_back');
const selectPage = document.querySelectorAll('.select_cam .selector');
let col_cam = [];
let cams = 'undefined';
let place = 'undefined';
let view_cams = 8;
let all_pages = 1;
let number_page = 1;
let n = 0;
let autoplay = true;

let vw = 0;

function loadCam(n, place) {

	let html = '';
	document.getElementById('cam').innerHTML = html;

	for (let i=0; i<view_cams; i++){
		if (i+n < place.length)
		html += `<div class="camera"><video id="my-video${i+n+1}" class="video-js vjs-default-skin" controls><source src="http://camera.lipetsk.ru/live/${place[i+n]}/playlist.m3u8" type="application/x-mpegURL"></video><div class="cam_title">Камера ${i+n+1} </div></div>`;
		else break;
	}
	document.getElementById('cam').innerHTML = html;

	if (number_page < all_pages)
		document.getElementById('camid').innerHTML = `Камеры ${n+1} - ${n+view_cams}`
	else
		document.getElementById('camid').innerHTML = `Камеры ${n+1} - ${place.length}`

	for (let i=0; i<view_cams; i++){
		if (i+n < place.length){
			if (autoplay){
				setTimeout(function() {
					col_cam[i] = videojs('#my-video'+(i+n+1), {muted: true});
					col_cam[i].play();
				}, 700 * (i + 1));
			}
			else
				col_cam[i] = videojs('#my-video'+(i+n+1));

		} else	break;
	}
}

function changeSource() {
	for (let i=0; i<col_cam.length; i++){
		if (col_cam[i] != ''){
			col_cam[i].pause();
			col_cam[i].dispose();
		}
		col_cam[i]='';
	}
	loadCam(n, place);
}

function backPage(){
	if ( number_page > 1){
		if (number_page === (all_pages))
			n = (number_page-2)*view_cams;
		else
			n -= view_cams;
		number_page -= 1;
		changeSource() ;
	}
}

function nextPage(){
	if ( number_page < all_pages){
			n += view_cams;
		number_page += 1;
		changeSource() ;
	}
}

function preload() {
	resizedw(window.innerWidth);

	cams = JSON.parse( json );
	btnsNext.onclick = nextPage;
	btnBack.onclick = backPage;
	place = cams[0]['byhanov'];
	all_pages = Math.ceil(place.length/view_cams);
	loadCam(n, place);

	btn_autoplay = document.querySelector('.switch-btn');
	
	//if (autoplay) btn_autoplay.classList.add('switch-on')
	//	else btn_autoplay.classList.remove('switch-on')

	btn_autoplay.addEventListener('click', function(){
			btn_autoplay.classList.toggle('switch-on');
			autoplay = !autoplay;
			localStorage.setItem('autoplay', autoplay);
			//if (autoplay)
			changeSource();

	});

	for (div of selectPage) {
		div.addEventListener('click', function(){
			for(a of selectPage){
				a.classList.toggle('active');
			}
				if (this.innerText === 'Быханов сад'){
					n = 0;
					number_page = 1;
					place = cams[0]['byhanov'];
					all_pages = Math.ceil(place.length/view_cams);
					changeSource();
				}else {
					n = 0;
					number_page = 1;
					place = cams[0]['gorodische'];
					all_pages = Math.ceil(place.length/view_cams);
					changeSource();
				}

		});
	}
}

preload();

let doit;
function resizedw(a){
	if (a >= 1600)
		view_cams = 8
	else if (a < 1600 && a > 1024)
		view_cams = 6
	else if (a <= 1024 && a > 767)
		view_cams = 4
	else if (a <= 767)
		view_cams = 2

	if (vw != 0 && vw != view_cams) {
		vw = view_cams;
		n = 0;
		number_page = 1;
		all_pages = Math.ceil(place.length/view_cams);
		changeSource();
	} else if (vw === 0)
		vw = view_cams;

}
window.onresize = function() {
    clearTimeout(doit);
    doit = setTimeout(function() {

        resizedw(window.innerWidth);
    }, 250);
};
